<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Rocked
 */
global $query_string;
query_posts("{$query_string}&posts_per_page=-1");
get_header(); ?>
<script>
	var filterRetos = function(){
		var $ = window.jQuery;
		var term = $('#retosFilter').val();
		console.log('Searching for ' + term);
		if(term === '') {
			$('.posts-layout .hentry').removeClass('hentry-hidden')
			$('.posts-layout .hentry').show();
		} else {
                        var terms = (term.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&") || '').
split(' ') || [];
                        var re = new RegExp('(' + terms.join('|') + ')', 'gi');
                        $('.posts-layout article').each(function(index, el){
                                var jEl = $(el);
                                var h2 = jEl.find('header h2');
                                var entry = jEl.find('.entry-summary');
                                var author = jEl.find('.vcard a');
                                if( (h2.length > 0 && re.exec(h2[0].innerHTML) ) || (entry.length > 0 &&
 re.exec(entry[0].innerHTML) ) || (author.length > 0 && re.exec(author[0].innerHTML) ) ) {
                                        // Show this entry
					jEl.show();
                                        jEl.removeClass('hentry-hidden')
                                } else {
                                        // Hide this entry
					jEl.hide();
                                        jEl.addClass('hentry-hidden')
                                }
                        });
			$('.posts-layout').masonry();
		}
	}
</script>


	<div id="primary" class="retos-main content-area col-md-9 <?php echo esc_attr(rocked_blog_layout()); ?>">
		<main id="main" class="content-wrap" role="main">
			<h2 class="entry-title">Retos</h2>
		<section>

<p>Llamamos <strong class="highlight">reto</strong> a cualquier desafío social que requiera una <strong class="highlight">solución</strong> tecnológica innovadora. Los retos son propuestos por cualquier individuo o asociación, publicados en esta web y están <strong class="highlight">asociados a los Premios y a la entidad</strong> que los concede. Los <strong class="highlight">hackers “for good”</strong> participantes formarán equipos para afrontar el reto elegido en busca de esa solución tecnológica.</p>
		<div>
			<a class="btn btn-primary" href="/nuevo-reto">Envía tu reto</a>
			<!-- <strong>El envío de retos ha finalizado!</strong> -->
		</div>

		<div class="input">
	 		<input id="retosFilter" type="text" placeholder="Filtrar retos..." onchange="filterRetos()" onkeydown="filterRetos()" onpaste="filterRetos()" oninput="filterRetos()" />
		</div>

		</section>

		<?php if ( have_posts() ) : ?>

			

			<?php /* Start the Loop */ ?>
			<div class="posts-layout">
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
					/* Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					//get_template_part( 'template-parts/content', get_post_format() );
					get_template_part( 'template-parts/content', 'retos' );
				?>

			<?php endwhile; ?>
			</div>

			<?php the_posts_navigation(); ?>

		<?php else : ?>

			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php 
	if ( get_theme_mod('blog_layout','classic') == 'classic' ) :
	get_sidebar();
	endif;
?>
<?php get_footer(); ?>
