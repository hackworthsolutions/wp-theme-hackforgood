<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Rocked
 */
?>

<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title">No hay contenido</h1>
	</header><!-- .page-header -->

	<div class="page-content">
		<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p><?php printf( wp_kses( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'rocked' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p>Lo siento, no encuentro nada buscando por esos términos. Por favor prueba con otros.</p>
			<?php get_search_form(); ?>

		<?php else : ?>

			<p>No encuentro la página que solicitas. Prueba a buscar.</p>
			<?php get_search_form(); ?>

		<?php endif; ?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
